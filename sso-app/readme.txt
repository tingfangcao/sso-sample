## tomcat8 jndi configuration
#classpath
add mysql dirver & c3p0 lib
#server.xml
add
<Resource
	factory="org.apache.naming.factory.BeanFactory"
	auth="Container"
    name="jdbc/sample"
	type="com.mchange.v2.c3p0.ComboPooledDataSource"
	description="sample DB Connection"
	maxPoolSize="50"
	minPoolSize="5"
	acquireIncrement="5"
	driverClass="com.mysql.jdbc.Driver"
	jdbcUrl="jdbc:mysql://localhost:3306/sample?useUnicode=true&amp;characterEncoding=utf-8&amp;useSSL=false"
	user="admin"
	password="Admin123"
	/>
#context.xml
add
<ResourceLink name="jdbc/sample" global="jdbc/sample"  type="javax.sql.DataSource"/>