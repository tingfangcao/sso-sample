<ul class="pagination">
	<li><span>总共 ${parameters.pager.pageCount} 页， ${parameters.pager.totalItems} 条记录</span></li>
	<#if parameters.pager.pageCount gt 0>	
		<#if parameters.pager.currentPage gt 1>
		<li><a href="#" onClick="goto(1)">&lt;&lt;</a></li>
		<li><a href="#" onClick="goto(${parameters.pager.currentPage-1})">&lt;</a></li>
		</#if>
		<#if parameters.pager.pageCount <= parameters.pager.pageLinkLimit>
			<#list 1..parameters.pager.pageCount as p>
			<li class="<#if p==parameters.pager.currentPage>active</#if>">
				<#if p==parameters.pager.currentPage>
				<a href="#"/>${p}</a>
				<#else><a href="#" onClick="goto(${p})"/>${p}</a>
				</#if>
			</li>
			</#list>
		<#else>
			<#list 1..(parameters.pager.pageLinkLimit-1)/2 as p>
			<li class="<#if p==parameters.pager.currentPage>active</#if>">
				<#if p==parameters.pager.currentPage>
				<a href="#"/>${p}</a>
				<#else><a href="#" onClick="goto(${p})"/>${p}</a>
				</#if>
			</li>
			</#list>
			<li><a href="#">&middot;&middot;&middot;</a></li>
			<#list parameters.pager.pageCount+1-(parameters.pager.pageLinkLimit-1)/2..parameters.pager.pageCount as p>
			<li class="<#if p==parameters.pager.currentPage>active</#if>">
				<#if p==parameters.pager.currentPage>
				<a href="#"/>${p}</a>
				<#else><a href="#" onClick="goto(${p})"/>${p}</a>
				</#if>
			</li>
			</#list>
		</#if>
		<#if parameters.pager.currentPage lt parameters.pager.pageCount>
		<li><a href="#" onClick="goto(${parameters.pager.currentPage+1})">&gt;</a></li>
		<li><a href="#" onClick="goto(${parameters.pager.pageCount})">&gt;&gt;</a></li>
		</#if>
	</#if>
</ul>
<script>
	function goto(p){
		document.getElementsByName('pager.currentPage')[0].value=p;
		//点击某一页就是提交一下表单，这样分页时查询条件就会自动带到下一页
		document.getElementById('${parameters.formId}').submit();
	}
</script>