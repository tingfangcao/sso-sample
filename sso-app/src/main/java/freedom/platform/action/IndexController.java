package freedom.platform.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;

@Controller
@Scope("prototype")
@Results({
    @Result(name="success", type="redirectAction", params = {"actionName" , "index"})
})
public class IndexController extends ActionSupport {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    // GET /index
    public HttpHeaders index() {
        return new DefaultHttpHeaders("index")  
            .disableCaching();  
    }
    
    // GET /welcome
    @Action(value = "welcome")
    public HttpHeaders welcome() {
        return new DefaultHttpHeaders("welcome")  
            .disableCaching();  
    }  
}
