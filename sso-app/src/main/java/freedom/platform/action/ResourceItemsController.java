package freedom.platform.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import freedom.platform.dao.ResourceItemDao;
import freedom.platform.enums.YesOrNo;
import freedom.platform.model.ResourceItem;
import freedom.platform.security.SecurityMetadataSource;
import freedom.utils.ExampleMatcherUtils;

@Controller
@Scope("prototype")
@Results({
    @Result(name="success", type="redirectAction", params = {"actionName" , "resource-items"})
})
public class ResourceItemsController extends ActionSupport implements ModelDriven<ResourceItem> {
	
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private SecurityMetadataSource securityMetadataSource;

    @Autowired
    private ResourceItemDao resourceItemDao;
    List<ResourceItem> parentResourceItems;
	
	private ResourceItem model;
	private List<ResourceItem> list;
	
    // GET /resourceItems  
	public HttpHeaders index() {
		if (model != null) {
			ExampleMatcher em = ExampleMatcher.matching();
			em = ExampleMatcherUtils.withIgnoreBlankValues(em, model);
			em = em.withMatcher(ResourceItem.PROP_NAME, GenericPropertyMatchers.contains());
			
			Example<ResourceItem> e = Example.of(model, em);
			List<Order> orders = new ArrayList<Order>(2);
			orders.add(new Order(Direction.ASC, ResourceItem.PROP_TYPE));
			orders.add(new Order(Direction.DESC, ResourceItem.PROP_UPDATE_TIME));
			Sort sort = new Sort(orders);
			list = resourceItemDao.findAll(e, sort);
			
//	        Context context = new HttpServletRequestContext(request);
//	        LimitFactory limitFactory = new TableLimitFactory(context);
//	        Limit limit = new TableLimit(limitFactory);
//	        request.setAttribute("list", list);
//	        request.setAttribute(TableConstants.TOTAL_ROWS, limit.getTotalRows());
		}
		return new DefaultHttpHeaders("index").disableCaching();
	}
    
    // GET /resourceItems/1  
    public HttpHeaders show() {
    	model = resourceItemDao.getOne(model.getId());
    	return new DefaultHttpHeaders("show");  
    }
    
    // GET /resourceItems/1/edit  
    public String edit() {  
    	model = resourceItemDao.getOne(model.getId());
        return "edit";  
    }  

    // GET /resourceItems/new  
    public String editNew() { 
        parentResourceItems = resourceItemDao.findByIsLeaf(YesOrNo.NO);
    	model = new ResourceItem();  
        return "editNew";  
    }  
  
    // GET /resourceItems/1/deleteConfirm  
    public String deleteConfirm() {  
        return "deleteConfirm";  
    }  
  
    // DELETE /resourceItems/1  
    public String destroy() {  
    	resourceItemDao.delete(model.getId());
    	securityMetadataSource.initAclCache();
        addActionMessage("ResourceItem removed successfully");  
        return "success";  
    }  
  
    // POST /resourceItems  
    public HttpHeaders create() {  
    	model.mark(true);
    	resourceItemDao.save(model);  
    	securityMetadataSource.initAclCache();
        addActionMessage("New resourceItem created successfully");  
        return new DefaultHttpHeaders("success")  
            .setLocationId(model.getId());  
    }  
  
    // PUT /resourceItems/1  
    public String update() {  
    	model.mark(false);
    	resourceItemDao.save(model);    
    	securityMetadataSource.initAclCache();
        addActionMessage("ResourceItem updated successfully");  
        return "success";  
    }  

    /* (non-Javadoc)
     * @see com.opensymphony.xwork2.ModelDriven#getModel()
     */
    @Override
    public ResourceItem getModel() {
    	if(null == model)
    		model = new ResourceItem();
    	return model;
    }

	/**
	 * @return the list
	 */
	public List<ResourceItem> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<ResourceItem> list) {
		this.list = list;
	}

    /**
     * @return the parentResourceItems
     */
    public List<ResourceItem> getParentResourceItems() {
        return parentResourceItems;
    }

    /**
     * @param parentResourceItems the parentResourceItems to set
     */
    public void setParentResourceItems(List<ResourceItem> parentResourceItems) {
        this.parentResourceItems = parentResourceItems;
    }
    
}
