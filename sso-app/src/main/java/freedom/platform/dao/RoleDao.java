package freedom.platform.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import freedom.platform.model.Role;

@Repository
public interface RoleDao extends JpaRepository<Role, Long> {

    Role findByName(String name);
}
