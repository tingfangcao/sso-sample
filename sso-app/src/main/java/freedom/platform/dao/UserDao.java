package freedom.platform.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import freedom.platform.model.User;

@Repository
public interface UserDao extends JpaRepository<User, Long> {

	User findByUsername(String username);
}
