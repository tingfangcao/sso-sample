package freedom.platform.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import freedom.platform.enums.ResourceItemType;
import freedom.platform.enums.YesOrNo;
import freedom.platform.model.ResourceItem;

@Repository
public interface ResourceItemDao extends JpaRepository<ResourceItem, Long> {

	/**
	 * @param resourceItemType
	 * @return
	 */
	List<ResourceItem> findByType(ResourceItemType resourceItemType);
	
	/**
	 * @param isLeaf
	 * @return
	 */
	List<ResourceItem> findByIsLeaf(YesOrNo isLeaf);
    /**
     * @param deleteFlag
     * @return
     */
    List<ResourceItem> findByDeleteFlag(YesOrNo deleteFlag);
    
    List<ResourceItem> findByDeleteFlagAndIsSecurity(YesOrNo deleteFlag, YesOrNo isSecurity);

}
