package freedom.platform.utils;

import java.sql.Timestamp;
import java.util.UUID;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import freedom.platform.model.User;

public class AppUtils {

	public static User currentUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(authentication == null || authentication.getPrincipal() == null)
			return null;
		Object principal = authentication.getPrincipal();
		if(principal instanceof User) {
			User user = (User) principal;
			return user;
		}
		else return null;
	}
	
	public static String currentUsername() {
		User user = currentUser();
		if(user != null)
			return user.getUsername();
		else return null;
	}
	
	public static Timestamp currentTime() {
		
		return new Timestamp(System.currentTimeMillis());
	}
	
	/**
	 * 通過UUID生成唯一的字符組合
	 * @return
	 */
	public static String getSingleString(){
		UUID ud = UUID.randomUUID();
		return ud.toString();
	}
}
