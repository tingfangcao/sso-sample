/**
 * 
 */
package freedom.platform.service;

import java.util.Collection;
import java.util.Map;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * @author charlie
 *
 */
public interface PermissionService {
	
	Map<RequestMatcher, Collection<ConfigAttribute>> obtainAclCache();

}
