/**
 * 
 */
package freedom.platform.service;

/**
 * @author charlie
 *
 */
public interface UserDetailsService extends org.springframework.security.core.userdetails.UserDetailsService {

}
