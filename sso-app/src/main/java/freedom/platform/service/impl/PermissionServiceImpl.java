/**
 * 
 */
package freedom.platform.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.annotation.Jsr250SecurityConfig;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import freedom.platform.dao.ResourceItemDao;
import freedom.platform.enums.YesOrNo;
import freedom.platform.model.ResourceItem;
import freedom.platform.service.PermissionService;

/**
 * @author charlie
 *
 */
@Service("permissionService")
public class PermissionServiceImpl implements PermissionService {

	@Autowired
	private ResourceItemDao resourceItemDao;
	
	@Override
	public Map<RequestMatcher, Collection<ConfigAttribute>> obtainAclCache() {
		Map<RequestMatcher, Collection<ConfigAttribute>> aclMap = new HashMap<RequestMatcher, Collection<ConfigAttribute>>();
		List<ResourceItem> permissionItems = resourceItemDao.findByDeleteFlagAndIsSecurity(YesOrNo.NO, YesOrNo.YES);
		
		for (ResourceItem permissionItem : permissionItems) {
		    if (StringUtils.hasText(permissionItem.getUrl())) {
		        RequestMatcher rm = new AntPathRequestMatcher(permissionItem.getUrl());
		        Collection<ConfigAttribute> list = new ArrayList<ConfigAttribute>(1);
		        list.add(new Jsr250SecurityConfig(String.valueOf(permissionItem.getId())));
		        aclMap.put(rm, list);
		    }
		}
		return aclMap;
	}

}
