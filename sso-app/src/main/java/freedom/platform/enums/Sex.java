package freedom.platform.enums;

import freedom.utils.enumplus.EnumPlus;

/**
 * @author charlie
 *
 */
public enum Sex implements EnumPlus {
    MALE(1), FEMALE(0);

    private Integer value;

    private Sex(Integer value) {
        this.value = value;
    }

    public static Sex[] all = {MALE, FEMALE};
    
    /**
     * @return the all
     */
    public static Sex[] getAll() {
        return all;
    }

    @Override
    public Object getValue() {
        return this.value;
    }
}
