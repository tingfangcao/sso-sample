package freedom.platform.enums;

import freedom.utils.enumplus.EnumPlus;

/**
 * @author charlie
 *
 */
public enum YesOrNo implements EnumPlus {
    YES(1), NO(0);

    private Integer value;

    private YesOrNo(Integer value) {
        this.value = value;
    }

    public static YesOrNo[] all = {YES, NO};
    
    /**
     * @return the all
     */
    public static YesOrNo[] getAll() {
        return all;
    }

    @Override
    public Object getValue() {
        return this.value;
    }

}
