package freedom.platform.enums;

import freedom.utils.enumplus.EnumPlus;

/**
 * @author charlie
 *
 */
public enum ResourceItemType implements EnumPlus {
    MENU(1), BUTTON(2), LINK(3), URL(4);

    private Integer value;

    private ResourceItemType(Integer value) {
        this.value = value;
    }

    public static ResourceItemType[] all = {MENU, BUTTON, LINK, URL};

    /**
     * @return the all
     */
    public static ResourceItemType[] getAll() {
        return all;
    }

    @Override
    public Object getValue() {
        return this.value;
    }

}
