package freedom.platform.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "A_TOKEN")
public class Token implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String REF = "PersistentLogins";
	
	public static final String PROP_USERNAME = "username";

	@Id
	@Column(length = 64, unique = true, nullable = false)
	private String id;

	@Column(name = "USERNAME", length = 64, unique = true, nullable = false)
	private String username;

	@Column(name = "TOKEN", length = 64, unique = false, nullable = false)
	private String token;

	@Column(name = "LAST_USED", unique = false, nullable = true, insertable = true, updatable = true)
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUsed;

	public Token(){super();};
	
	public Token(String series, String username, String token, Date lastUsed) {
		super();
		this.id = series;
		this.username = username;
		this.token = token;
		this.lastUsed = lastUsed;
	}

	public String getSeries() {
		return id;
	}

	public void setSeries(String series) {
		this.id = series;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getLastUsed() {
		return lastUsed;
	}

	public void setLastUsed(Date lastUsed) {
		this.lastUsed = lastUsed;
	}

}
