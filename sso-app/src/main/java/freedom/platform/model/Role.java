package freedom.platform.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "A_ROLE")
public class Role extends BaseModel {

	private static final long serialVersionUID = 1L;

	public static final String PROP_NAME = "name";
	public static final String PROP_DESCRIPTION = "description";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;
	
    @Column(length=64, nullable=false, unique = true)  
    private String name; 
    
    @Column(length=255)
    private String description;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "A_USER_ROLE",
	joinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName = "ID")},
	inverseJoinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName ="ID")})
    private Collection<User> users;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "A_ROLE_RESOURCE_ITEM",
	joinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName = "ID")},
	inverseJoinColumns = {@JoinColumn(name = "RESOURCE_ITEM_ID", referencedColumnName ="ID")})
	private Collection<ResourceItem> resourceItems;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Collection<User> getUsers() {
		return users;
	}

	public void setUsers(Collection<User> users) {
		this.users = users;
	}

	public Collection<ResourceItem> getResourceItems() {
		return resourceItems;
	}

	public void setResourceItems(Collection<ResourceItem> resourceItems) {
		this.resourceItems = resourceItems;
	}

}