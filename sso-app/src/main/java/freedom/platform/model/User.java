package freedom.platform.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import freedom.platform.enums.Sex;
import freedom.platform.enums.YesOrNo;

@Entity
@Table(name = "A_USER")
public class User extends BaseModel {
	private static final long serialVersionUID = 1L;
	
	public static final String REF = "User";
	public static final String PROP_ID = "id";
	public static final String PROP_USERNAME = "username";
	public static final String PROP_PASSWORD = "password";
	public static final String PROP_PHONE = "phone";
	public static final String PROP_EMAIL = "email";
	public static final String PROP_EXPIRY_DATE = "expiryDate";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, updatable = false)
	private Long id;

	@Column(length = 64, unique = true, nullable = false)
	private String username;

	@Column(length = 128, nullable = false)
	private String password;

	@Column(length = 14)
	private String phone;

	@Column(length = 128)
	private String email;
	
	@Column(name = "EXPIRY_DATE")
	private Date expiryDate;
	
	@Column(name = "LOCK_FLAG", length = 1, nullable = false)
	private YesOrNo lockFlag;
	
	@Column(length=1)
	private Sex sex;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "A_USER_ROLE",
	joinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "ID")},
	inverseJoinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName ="ID")})
	private Collection<Role> roles;
	
	public User() {
		super();
	}

	/**
	 * 
	 */
	public User(User user) {
		super();
		BeanUtils.copyProperties(user, this);
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return the lockFlag
	 */
	public YesOrNo getLockFlag() {
		return lockFlag;
	}

	/**
	 * @param lockFlag the lockFlag to set
	 */
	public void setLockFlag(YesOrNo lockFlag) {
		this.lockFlag = lockFlag;
	}


	/**
	 * @return the sex
	 */
	public Sex getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(Sex sex) {
		this.sex = sex;
	}

	/**
	 * @return the roles
	 */
	public Collection<Role> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

}
