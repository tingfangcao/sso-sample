
package freedom.platform.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import freedom.platform.enums.ResourceItemType;
import freedom.platform.enums.YesOrNo;

@Entity  
@Table(name="A_RESOURCE_ITEM")  
public class ResourceItem extends BaseModel {  
   
	private static final long serialVersionUID = 1L;
	public static final String REF = "ResourceItem";
	public static final String PROP_ID = "id";
	public static final String PROP_NAME = "name";
	public static final String PROP_TYPE = "type";
	public static final String PROP_DESCRIPTION = "description";
    public static final String PROP_URL = "url";
    public static final String PROP_IS_ROOT = "isRoot";
    public static final String PROP_IS_LEAF = "isLeaf";
    public static final String PROP_PAREND_ID = "parendId";

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)  
    @Column(name="ID", unique=true, nullable=false)  
    private Long id;
   
    @Column(length=64, nullable=false)  
    private String name; 
    
    @Column(length=255)
    private String description;
    
    @Column(length=1, nullable=false)  
    private ResourceItemType type; 
    
    @Column(length=255)  
    private String url;
    
    @Column(name = "is_root", length = 1, nullable = false)
	private YesOrNo isRoot;
    
    @Column(name = "is_leaf", length = 1, nullable = false)
    private YesOrNo isLeaf;
    
    @Column(name = "is_security", length = 1, nullable = false)
    private YesOrNo isSecurity;
    
    @Column(name="parent_id")
    private Long parentId;
    
	/**
     * @return the parentId
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parentId to set
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", referencedColumnName = "ID", insertable=false, updatable=false)
	private ResourceItem parent;
    
	@OneToMany(mappedBy="parent", fetch = FetchType.LAZY)
    private Collection<ResourceItem> children;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ResourceItemType getType() {
		return type;
	}

	public void setType(ResourceItemType type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public YesOrNo getIsRoot() {
		return isRoot;
	}

	public void setIsRoot(YesOrNo isRoot) {
		this.isRoot = isRoot;
	}

	public YesOrNo getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(YesOrNo isLeaf) {
		this.isLeaf = isLeaf;
	}

	public Long getParendId() {
		return parentId;
	}

	public void setParendId(Long parendId) {
		this.parentId = parendId;
	}

	/**
	 * @return the parent
	 */
	public ResourceItem getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(ResourceItem parent) {
		this.parent = parent;
	}

	/**
	 * @return the children
	 */
	public Collection<ResourceItem> getChildren() {
		return children;
	}

	/**
	 * @param children the children to set
	 */
	public void setChildren(Collection<ResourceItem> children) {
		this.children = children;
	}

    /**
     * @return the isSecurity
     */
    public YesOrNo getIsSecurity() {
        return isSecurity;
    }

    /**
     * @param isSecurity the isSecurity to set
     */
    public void setIsSecurity(YesOrNo isSecurity) {
        this.isSecurity = isSecurity;
    }

}