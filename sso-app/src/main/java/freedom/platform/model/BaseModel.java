package freedom.platform.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

import org.springframework.beans.BeanUtils;

import freedom.platform.enums.YesOrNo;
import freedom.platform.utils.AppUtils;

@MappedSuperclass
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String REF = "Mark";
	public static final String PROP_DELETE_FLAG = "deleteFlag";
	public static final String PROP_CREATE_USER = "createUser";
	public static final String PROP_CREATE_TIME = "createTime";
	public static final String PROP_UPDATE_USER = "updateUser";
	public static final String PROP_UPDATE_TIME = "updateTime";
	
	@Column(name = "DELETE_FLAG", length = 1, nullable = false)
	private YesOrNo deleteFlag;

	@Column(name = "CREATE_USER", length = 64, unique = false, nullable = true)
	private String createUser;

	@Column(name = "CREATE_TIME", columnDefinition = "timestamp(6) null")
	private Timestamp createTime;

	@Column(name = "UPDATE_USER", length = 64, unique = false, nullable = true)
	private String updateUser;

	@Column(name = "UPDATE_TIME", columnDefinition = "timestamp(6) null")
	private Timestamp updateTime;
	
	/**
	 * 
	 */
	public BaseModel() {
		super();
	}

	public BaseModel(BaseModel mark) {
		super();
		BeanUtils.copyProperties(mark, this);
	}

	public YesOrNo getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(YesOrNo deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
	
	/**
	 * 批量赋值
	 * @param isCreate true插入，false更新
	 */
	public final void mark (boolean isCreate) {
		
		this.updateUser = AppUtils.currentUsername();
		this.updateTime = AppUtils.currentTime();
		if (isCreate) {
			this.createUser = this.updateUser;
			this.createTime = this.updateTime;
			this.deleteFlag = YesOrNo.NO;
		}
	}
	
}
