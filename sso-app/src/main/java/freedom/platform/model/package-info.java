@TypeDefs(value = {
    @TypeDef(defaultForType = YesOrNo.class, typeClass = freedom.utils.enumplus.ValueEnumType.class),
    @TypeDef(defaultForType = Sex.class, typeClass = freedom.utils.enumplus.ValueEnumType.class),
    @TypeDef(defaultForType = ResourceItemType.class, typeClass = freedom.utils.enumplus.ValueEnumType.class)}) 
package freedom.platform.model;

import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import freedom.platform.enums.ResourceItemType;
import freedom.platform.enums.Sex;
import freedom.platform.enums.YesOrNo;
