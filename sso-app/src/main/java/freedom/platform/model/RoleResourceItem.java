package freedom.platform.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "A_ROLE_RESOURCE_ITEM", uniqueConstraints={@UniqueConstraint(columnNames={"ROLE_ID", "RESOURCE_ITEM_ID"})})
public class RoleResourceItem implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROLE_ID", referencedColumnName ="ID", nullable=false)
	private Role role;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "RESOURCE_ITEM_ID", referencedColumnName = "ID", nullable=false)
	private ResourceItem resourceItem;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * @return the resourceItem
     */
    public ResourceItem getResourceItem() {
        return resourceItem;
    }

    /**
     * @param resourceItem the resourceItem to set
     */
    public void setResourceItem(ResourceItem resourceItem) {
        this.resourceItem = resourceItem;
    }
	

}