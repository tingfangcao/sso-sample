package freedom.utils.enumplus;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class EnumPlusSerializer extends JsonSerializer<EnumPlus> {
	
	public final static EnumPlusSerializer instance = new EnumPlusSerializer();

	@Override
	public void serialize(EnumPlus value, JsonGenerator generator, SerializerProvider serializers) throws IOException {
		generator.writeString(String.valueOf(value.getValue()));
	}
}
