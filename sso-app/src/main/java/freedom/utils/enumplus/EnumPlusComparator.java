package freedom.utils.enumplus;

import java.util.Comparator;

public class EnumPlusComparator {
	
	public static Comparator<EnumPlus> instance = new Comparator<EnumPlus>() {

		public int compare(EnumPlus o1, EnumPlus o2) {
			if (o1.getValue().hashCode() == o2.getValue().hashCode())
				return 0;
			else if (o1.getValue().hashCode() > o2.getValue().hashCode())
				return 1;
			else
				return -1;
		}
	};

}
