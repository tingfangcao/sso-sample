package freedom.utils.enumplus;

import java.io.IOException;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class EnumPlusDeserializer extends JsonDeserializer<EnumPlus> {
	public final static EnumPlusDeserializer instance = new EnumPlusDeserializer();

	@Override
	public EnumPlus deserialize(JsonParser jp, DeserializationContext dctx) throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
		String currentName = jp.getCurrentName();
		Object currentValue = jp.getCurrentValue();
		@SuppressWarnings("unchecked")
		Class<EnumPlus> propType = (Class<EnumPlus>) BeanUtils.findPropertyType(currentName, currentValue.getClass());
//		JsonFormat annotation = (JsonFormat) findPropertyType.getAnnotation(JsonFormat.class);
//		DbEnum dbEnum = DbEnum.fromValue(findPropertyType, node.get(DbEnum.PROP_VALUE).asText());
		EnumPlus enumPlus = EnumPlus.fromValue(propType, node.asText());
		return enumPlus;
	}

}
