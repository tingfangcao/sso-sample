package freedom.utils.enumplus;

import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

/**
 * @author Charlie Email:tingfangcao@163.com
 * @date 2020/08/13
 */
public class EnumPlusConverter extends StrutsTypeConverter {

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public Object convertFromString( Map context, String[] values, Class clazz) {
        String value = values[0];
        return EnumPlus.fromValue(clazz, value);
    }

    @SuppressWarnings({"rawtypes"})
    @Override
    public String convertToString(Map context, Object o) {
        return null != o && o instanceof EnumPlus ? ((EnumPlus)o).getValue().toString() : "";
    }

}
