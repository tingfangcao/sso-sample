/**
 * 
 */
package freedom.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author charlie
 *
 */
public class DateUtils {
	
	public static final String PATTERN_DATE = "yyyyMMDD";
	public static final String PATTERN_TIME = "HH:hh:ss";
	public static final String PATTERN_TIMESTAMP = "yyyyMMDD HH:hh:ss.sss";
	
	public static int compare(String pattern, Date d1, Date d2) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.parse(sdf.format(d1)).compareTo(sdf.parse(sdf.format(d2)));
	}
	
	public static boolean equal(String pattern, Date d1, Date d2) throws ParseException {
		return compare(pattern, d1, d2) == 0;
	}
	
	public static boolean before(String pattern, Date d1, Date d2) throws ParseException {
		return compare(pattern, d1, d2) < 0;
	}
	
	public static boolean after(String pattern, Date d1, Date d2) throws ParseException {
		return compare(pattern, d1, d2) > 0;
	}

}
