package freedom.struts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.ComponentTag;

import com.opensymphony.xwork2.util.ValueStack;

/**
 * @author Charlie Email:tingfangcao@163.com
 * @date 2020/09/03
 */
public class PaginationTag extends ComponentTag {

    private static final long serialVersionUID = 1L;

    private String pager;
    private String formId;

    public String getPager() {
        return pager;
    }

    public void setPager(String pager) {
        this.pager = pager;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    @Override
    public Component getBean(ValueStack stack, HttpServletRequest req, HttpServletResponse res) {
        return new Pagination(stack, req, res);
    }

    protected void populateParams() {
        super.populateParams();

        Pagination pagination = ((Pagination)component);
        pagination.setFormId(formId);
        pagination.setPager(pager);

    }
}
