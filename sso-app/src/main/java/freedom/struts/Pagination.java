package freedom.struts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.UIBean;
import org.apache.struts2.views.annotations.StrutsTag;

import com.opensymphony.xwork2.util.ValueStack;

/**
 * @author Charlie Email:tingfangcao@163.com
 * @date 2020/09/03
 */
@StrutsTag(name = "pagination", tldTagClass = "freedom.struts.PaginationTag",
    description = "Render a pagination component", allowDynamicAttributes = true)
public class Pagination extends UIBean {// 继承自Struts2的标签Bean
    private String pager;// 分页对象
    private String formId;// 查询时需要提交的表单ID

    public Pagination(ValueStack stack, HttpServletRequest request, HttpServletResponse response) {
        super(stack, request, response);
    }

    private static final String TEMPLATE = "pagination";

    @Override
    protected String getDefaultTemplate() {
        return TEMPLATE;
    }

    @Override
    protected void evaluateExtraParams() {
        // 这个函数作用在与把值添加到valueStack中，这样在页面上就可以直接通过${parameters.pager.pageCount}调用
        if (pager != null) {
            addParameter("pager", findValue(pager));
        }
        if (formId != null) {
            addParameter("formId", findString(formId));
        }
    }

    public String getPager() {
        return pager;
    }

    public void setPager(String pager) {
        this.pager = pager;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }
}
