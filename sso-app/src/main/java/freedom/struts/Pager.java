package freedom.struts;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * @author Charlie Email:tingfangcao@163.com
 * @date 2020/09/03
 */
public class Pager<T> {

    public PageRequest transform() {
        return new PageRequest(currentPage - 1, pageSize);
    }
    
    public PageRequest transform(Sort sort) {
        return new PageRequest(currentPage - 1, pageSize, sort);
    }
    
    public void complete(Page<T> page) {
        this.totalItems = page.getTotalElements();
        this.pageCount = page.getTotalPages();
        this.content = page.getContent();
    }

    // 当前页号
    private int currentPage;
    // 单页项数
    private int pageSize;
    // 页链接数限制
    private int pageLinkLimit = 9;
    // 总页数
    private int pageCount;
    // 总项数
    private long totalItems;
    
    List<T> content = new ArrayList<>(0);

    public Pager() {
        this(1, 10, 9); //默认
    }

    /**
     * @param currentPage 当前页，从1开始
     * @param pageSize 单页项数
     * @param pageLinkLimit 页链接上限，未超过则全部展示，超过则省略
     */
    public Pager(int currentPage, int pageSize, int pageLinkLimit) {
        super();
        this.currentPage = currentPage > 0 ? currentPage : 1;
        this.pageSize = pageSize;
        this.pageLinkLimit = pageLinkLimit;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageCount() {
        return pageCount;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public List<T> getContent() {
        return content;
    }

    public int getPageLinkLimit() {
        return pageLinkLimit;
    }

}
