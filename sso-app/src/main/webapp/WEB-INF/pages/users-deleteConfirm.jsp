<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html lang="en">
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
<body>
	<s:form cssClass="center" action="/users/%{id}/destroy" method="get">
		<s:hidden name="_method" value="delete"/>
		<s:hidden name="_csrf" value="%{#request._csrf.token}"/>
		<s:hidden name="_csrf_header" value="%{#request._csrf.headerName}"/>
		<s:text name="sure.delete" />
		<s:submit cssClass="btn btn-primary" value="%{getText('delete')}"/>
		<s:a cssClass="btn btn-primary" value="/users"><s:text name='cancel' /></s:a>
		<s:actionerror/>
		<s:actionmessage/>
	</s:form>
</body>
</html>