<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html lang="en">
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
    <div>
        <s:text name="welcome" />
    </div>
    <div>
        <s:actionerror />
        <s:actionmessage />
    </div>
    <h1>
        <s:text name="welcome" />
    </h1>
</body>
</html>