<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html lang="en">
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
	<div class="container" id="userManagement-view">
		<s:label for="userManagement-view">
			<s:text name="userManagement"></s:text>-<s:text name="view"></s:text>
		</s:label>
		<div class="row">
			<div class="col-xs-4 col-sm-2">
				<label><s:text name="username"></s:text></label>
			</div>
			<div class="col-xs-8 col-sm-4">
				<s:property value="username" />
			</div>
			<div class="col-xs-4 col-sm-2">
				<label><s:text name="email"></s:text></label>
			</div>
			<div class="col-xs-8 col-sm-4">
				<s:property value="email" />
			</div>
		</div>
	<hr>
	<div class="row">
		<div class="col-xs-4 col-sm-2">
			<label><s:text name="sequence"></s:text></label>
		</div>
		<div class="col-xs-4 col-sm-2">
			<label><s:text name="role.name"></s:text></label>
		</div>
		<div class="col-xs-4 col-sm-2">
			<label><s:text name="role.description"></s:text></label>
		</div>
	</div>	
	<s:iterator value="roles" var="item" status="st">
		<div class="row">
			<div class="col-xs-4 col-sm-2">
				<s:property value="#st.count" />
			</div>
			<div class="col-xs-4 col-sm-2">
				<s:property value="name" />
			</div>
			<div class="col-xs-4 col-sm-2">
				<s:property value="description" />
			</div>
		</div>
	</s:iterator>
		<div class="row">
			<s:a cssClass="btn btn-primary" value="/users">
				<s:text name='back' />
			</s:a>
		</div>
		<div class="row">
			<s:actionerror />
			<s:actionmessage />
		</div>
	</div>
</body>
</html>