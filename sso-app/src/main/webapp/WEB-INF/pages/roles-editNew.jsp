<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html lang="en">
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
	<s:label for="roleManagement-new">
		<s:text name="roleManagement"></s:text>-<s:text name="new"></s:text>
	</s:label>
	<s:form id="roleManagement-new" cssClass="form-horizontal" role="form" validate="true"
		 method="post" action="/roles">
		<s:hidden name="_csrf" value="%{#request._csrf.token}"/>
		<s:hidden name="_csrf_header" value="%{#request._csrf.headerName}"/>
		<s:token/>
		
		<s:textfield name="name" key="role.name" />
		<s:textfield name="description" key="role.description" />
		
	    <s:submit cssClass="btn btn-primary" value="%{getText('save')}"/>
	    <s:a cssClass="btn btn-primary" value="/roles"><s:text name='cancel' /></s:a>
		<s:actionerror/>
		<s:actionmessage/>
	</s:form>
</body>
</html>