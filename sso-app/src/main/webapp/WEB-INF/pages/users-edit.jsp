<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html lang="en">
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
	<s:label for="userManagement-edit">
		<s:text name="userManagement"></s:text>-<s:text name="edit"></s:text>
	</s:label>
	<s:form id="userManagement-edit" cssClass="form-horizontal" role="form"
		 method="post" action="/users/%{id}">
		<s:hidden name="_method" value="put"/>
		<s:hidden name="_csrf" value="%{#request._csrf.token}"/>
		<s:hidden name="_csrf_header" value="%{#request._csrf.headerName}"/>
		<s:hidden name="id"/>
		<s:hidden name="createTime"/>
		<s:hidden name="createUser"/>
		
		<s:textfield 
		        key="email"
				type="email"
				placeholder="%{getText('email')}"
		        />
		<s:textfield
		        key="username"
		        />
		<s:password
		        key="password"
		        />
		<s:select
		        key="sex" value="%{sex.value}" list="@freedom.platform.enums.Sex@getAll()" listKey="value" listValue="display" emptyOption="true"
		        />
		<s:select
		        key="lockFlag" value="%{lockFlag.value}" list="@freedom.platform.enums.YesOrNo@getAll()" listKey="value" listValue="display" emptyOption="true"
		        />
		<s:select
		        key="deleteFlag" value="%{deleteFlag.value}" list="@freedom.platform.enums.YesOrNo@getAll()" listKey="value" listValue="display" emptyOption="true"
		        />
		
	    <s:submit cssClass="btn btn-primary" value="%{getText('save')}"/>
	    <s:a cssClass="btn btn-primary" value="/users"><s:text name='cancel' /></s:a>
		<s:actionerror/>
		<s:actionmessage/>
	</s:form>
</body>
</html>