<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html lang="en">
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
	<s:label for="resourceItemManagement-edit">
		<s:text name="resourceItemManagement"></s:text>-<s:text name="edit"></s:text>
	</s:label>
	<s:form id="resourceItemManagement-edit" role="form" cssClass="form-horizontal"
	 method="post" action="/resource-items/%{id}">
		<s:hidden name="_method" value="put" />
		<s:hidden name="_csrf" value="%{#request._csrf.token}" />
		<s:hidden name="_csrf_header" value="%{#request._csrf.headerName}" />
		<s:token/>
		
		<s:hidden name="id"/>
		<s:hidden name="createTime"/>
		<s:hidden name="createUser"/>
		<s:hidden name="deleteFlag" value="%{deleteFlag.value}"/>
		
		<s:textfield name="name" key="resourceItem.name" placeholder="%{getText('resourceItem.name')}" />
		<s:select name="type" value="%{type.value}" key="resourceItem.type" list="@freedom.platform.enums.ResourceItemType@getAll()" listKey="value" listValue="display" emptyOption="true"/>
		<s:textfield name="description" key="resourceItem.description" />
		<s:select name="isRoot" value="%{isRoot.value}" key="resourceItem.isRoot" list="@freedom.platform.enums.YesOrNo@getAll()" listKey="value" listValue="display" emptyOption="true"/>
		<s:select name="isLeaf" value="%{isLeaf.value}" key="resourceItem.isLeaf" list="@freedom.platform.enums.YesOrNo@getAll()" listKey="value" listValue="display" emptyOption="true"/>
		<s:textfield name="url" key="resourceItem.url" />
		<s:select name="isSecurity" value="%{isSecurity.value}" key="resourceItem.isSecurity" list="@freedom.platform.enums.YesOrNo@getAll()" listKey="value" listValue="display" emptyOption="true"/>
		
		<s:submit cssClass="btn btn-primary" value="%{getText('save')}" />
		<s:a cssClass="btn btn-primary" value="/resource-items"><s:text name='cancel' /></s:a>
		<s:actionerror />
		<s:actionmessage />
	</s:form>
</body>
</html>