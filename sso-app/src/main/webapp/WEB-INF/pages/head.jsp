<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>
<%@ taglib prefix="sp" uri="/struts-pagination-tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%
    String scheme = request.getScheme();
    String path = request.getContextPath();
    String basePath = scheme + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    String showPath = scheme + "://www.51yunhui.com/kms/";
    String wsScheme = "http".equals(scheme) ? "ws" : "wss";
    String baseWsPath = wsScheme + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<c:set var="scheme" value="<%=scheme%>"></c:set>
<c:set var="path" value="<%=path%>"></c:set>
<c:set var="basePath" value="<%=basePath%>"></c:set>
<c:set var="showPath" value="<%=showPath%>"></c:set>
<c:set var="baseWsPath" value="<%=baseWsPath%>"></c:set>
<security:authentication property="principal" var="principal" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<c:url value='/images/logo.ico'/>">
<s:head theme="simple" />
<sj:head compressed="true" jquery="true" />
<sb:head compressed="true" includeScripts="true" includeScriptsValidation="true" includeStyles="true" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="<c:url value='/struts/bootstrap/css/bootstrap-theme.min.css'/>">
<link rel="stylesheet" href="<c:url value='/css/app.css'/>">
<link rel="stylesheet" href="<c:url value='/css/animate.css'/>">
<link rel="stylesheet" href="<c:url value='/css/bootsnav.css'/>">
<link rel="stylesheet" href="<c:url value='/css/style.css'/>">
<script src="<c:url value='/js/app.js'/>"></script>
<script src="<c:url value='/js/bootsnav.js'/>"></script>
<script src="<c:url value='/js/pym.v1.js'/>"></script>
