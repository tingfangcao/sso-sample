<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html>
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
	<s:label for="resourceItemManagement"><s:text name="resourceItemManagement"></s:text></s:label>
	<div id="resourceItemManagement">
		<s:label for="condition"><s:text name="condition"></s:text></s:label>
		<s:form id="condition" cssClass="form-horizontal" role="form"
			 method="get" action="/resource-items">
			 
			<s:textfield  id="name" name="name" key="resourceItem.name" placeholder="%{getText('resourceItem.name')}" />
			<s:select id="type" name="type" value="%{type.value}" key="resourceItem.type" list="@freedom.platform.enums.ResourceItemType@getAll()" emptyOption="true"
			 listKey="value" listValue="display"></s:select>
			 
			<div class="col-sm-offset-2 col-sm-10">
				<s:submit cssClass="btn btn-primary" value="%{getText('query')}" />
				<s:reset cssClass="btn btn-primary" value="%{getText('reset')}" />
				<s:a cssClass="btn btn-primary" value="/resource-items/new">
					<s:text name="new" />
				</s:a>
			</div>
			<div class="col-sm-12">
				<s:actionerror />
				<s:actionmessage />
			</div>
		</s:form>
	<div class="table-responsive">
		<ec:tree
		identifier="id"
		parentAttribute="parentId"
		items="list"
		var="it"
		view="org.extremecomponents.tree.TreeView"
		filterable="false"
		sortable="false"
		styleClass="table"
		>
		  <ec:row>
		    <ec:column property="#" title="sequence">${ROWCOUNT}</ec:column>
		    <ec:column property="name" title="resourceItem.name" cell="org.extremecomponents.tree.TreeCell"/>
		    <ec:column property="type" title="resourceItem.type"/>
		    <ec:column property="description" title="resourceItem.description"/>
		    <ec:column property="url" title="resourceItem.url"/>
		    <ec:column property="isSecurity" title="resourceItem.isSecurity"/>
		    <ec:column property="deleteFlag" title="deleteFlag"/>
		    <ec:column property="operation" title="operation" >
		    	<a href="resource-items/${it.id}/edit" class="btn btn-default">编辑</a>
				<a href="resource-items/${it.id}/deleteConfirm" onclick="return js_confirm();" class="btn btn-default">删除</a>
	        </ec:column>
		  </ec:row>
		</ec:tree>
	</div>
	</div>
</body>
</html>