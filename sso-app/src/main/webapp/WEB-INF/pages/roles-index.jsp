<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html>
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<s:form id="condition" role="form" method="get" action="/roles">

				<div class="form-group">
					<div class="col-sm-4">
						<s:textfield  key="name" label="%{getText('role.name')}" placeholder="%{getText('role.name')}" cssClass="form-control" />
					</div>
				</div>
				
				<div class="col-sm-offset-2 col-sm-10">
					<s:submit cssClass="btn btn-primary" value="%{getText('query')}" />
					<s:reset cssClass="btn btn-primary" value="%{getText('reset')}" />
					<s:a cssClass="btn btn-primary" value="/roles/new">
						<s:text name="new" />
					</s:a>
				</div>
				<div class="col-sm-12">
					<s:actionerror />
					<s:actionmessage />
				</div>
			</s:form>
		</div>
		<div class="table-responsive">
			<table class="table">
				<caption>
					<s:text name="result" />
				</caption>
				<thead>
					<tr>
						<th><s:text name="sequence" /></th>
						<th><s:text name="role.name" /></th>
						<th><s:text name="role.description" /></th>
						<th><s:text name="operation" /></th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="list" status="st">
						<tr>
							<td><s:property value='#st.count' /></td>
							<td><s:property value='name' /></td>
							<td><s:property value='description' /></td>
							<td><a href="<s:url value='roles/%{id}' />"><s:text name="view" /></a> | <a
								href="<s:url value='roles/%{id}/edit' />"><s:text name="edit" /></a> | <a
								href="<s:url value='roles/%{id}/deleteConfirm' />"><s:text name="delete" /></a></td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>