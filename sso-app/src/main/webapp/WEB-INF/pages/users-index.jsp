<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html lang="en">
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<s:form id="condition" role="form" method="get" action="/users">
				<s:hidden name="pager.currentPage" />
				<div class="col-md-6">
					<s:textfield key="user.username" placeholder="%{getText('user.username')}" />
				</div>
				<div class="col-md-6">
					<s:textfield key="user.phone" placeholder="%{getText('user.phone')}" />
				</div>
				<div class="col-md-6">
					<s:select key="user.sex" value="%{user.sex.value}" list="@freedom.platform.enums.Sex@getAll()" listKey="value" 
						listValue="getText('freedom.platform.enums.Sex.'+value)" headerKey="" headerValue="%{getText('no_select')}" />
				</div>
				<div class="col-md-offset-3 col-md-9">
					<s:submit value="%{getText('query')}" cssClass="btn btn-primary" id="query"/>
					<s:reset value="%{getText('reset')}" cssClass="btn btn-primary"/>
					<s:a value="/users/new" cssClass="btn btn-primary mr-2"><s:text name="new" /></s:a>
					<s:a value="/users/deleteBatch" cssClass="btn btn-primary mr-2"><s:text name="deleteBatch" /></s:a>
				</div>
				<div class="col-md-12">
					<s:actionerror />
					<s:actionmessage />
				</div>
			</s:form>
		</div>
		<div class="row">
		</div>
		<div class="row pull-right">
			<sp:pagination pager="pager" formId="condition"/>
		</div>
		<div class="row">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th><s:text name="sequence" /></th>
						<th><s:text name="user.username" /></th>
						<th><s:text name="user.email" /></th>
						<th><s:text name="user.phone" /></th>
						<th><s:text name="user.sex" /></th>
						<th><s:text name="lockFlag" /></th>
						<th><s:text name="deleteFlag" /></th>
						<th><s:text name="operation" /></th>
					</tr>
				</thead>
				<tbody>
					<s:iterator value="pager.content" status="st">
						<tr>
							<td><s:checkbox theme="simple" key="checkedIds" label="%{id}" value="%{id % 2 == 0}" fieldValue="%{id}"/></td>
							<td><s:property value='#st.count' /></td>
							<td><s:property value='username' /></td>
							<td><s:property value='email' /></td>
							<td><s:property value='phone' /></td>
							<td><s:property value="getText('freedom.platform.enums.Sex.'+sex.value)" /></td>
							<td><s:property value="getText('freedom.platform.enums.YesOrNo.'+lockFlag.value)" /></td>
							<td><s:property value="getText('freedom.platform.enums.YesOrNo.'+lockFlag.value)" /></td>
							<td><s:a value="/users/%{id}">
									<s:text name="view" />
								</s:a>| <s:a value="/users/%{id}/edit">
									<s:text name="edit" />
								</s:a>| <s:a value="/users/%{id}/deleteConfirm">
									<s:text name="delete" />
								</s:a></td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
		</div>

	</div>
	<script>
		var pymChild = new pym.Child();
		$('#query').click(function(){
			pymChild.sendHeight();
		});
	</script>
</body>
</html>