<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html lang="en">
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
	<h3>
		<s:text name="error" />
	</h3>
	<div>
		<s:actionerror />
		<s:actionmessage />
	</div>
</body>
</html>