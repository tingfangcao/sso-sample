<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html lang="en">
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
<body>
	<div class="container" id="resourceItemManagement-view">
		<s:label for="resourceItemManagement-view">
			<s:text name="resourceItemManagement"></s:text>-<s:text name="view"></s:text>
		</s:label>
		<div class="row">
			<div class="col-xs-4 col-sm-2">
				<label><s:text name="resourceItem.name"></s:text></label>
			</div>
			<div class="col-xs-8 col-sm-4">
				<s:property value="name" />
			</div>
			<div class="col-xs-4 col-sm-2">
				<label><s:text name="resourceItem.type"></s:text></label>
			</div>
			<div class="col-xs-8 col-sm-4">
				<s:property value="type.display" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 col-sm-2">
				<label><s:text name="resourceItem.isRoot"></s:text></label>
			</div>
			<div class="col-xs-8 col-sm-4">
				<s:property value="isRoot.display" />
			</div>
			<div class="col-xs-4 col-sm-2">
				<label><s:text name="resourceItem.isLeaf"></s:text></label>
			</div>
			<div class="col-xs-8 col-sm-4">
				<s:property value="isLeaf.display" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 col-sm-2">
				<label><s:text name="resourceItem.url"></s:text></label>
			</div>
			<div class="col-xs-8 col-sm-4">
				<s:property value="url" />
			</div>
			<div class="col-xs-4 col-sm-2">
				<label><s:text name="resourceItem.isSecurity"></s:text></label>
			</div>
			<div class="col-xs-8 col-sm-4">
				<s:property value="isSecurity.display" />
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 col-sm-2">
				<label><s:text name="resourceItem.description"></s:text></label>
			</div>
			<div class="col-xs-8 col-sm-4">
				<s:property value="description" />
			</div>
			<div class="col-xs-4 col-sm-2">
				<label><s:text name="deleteFlag"></s:text></label>
			</div>
			<div class="col-xs-8 col-sm-4">
				<s:property value="deleteFlag.display" />
			</div>
		</div>
		<div class="row">
			<s:a cssClass="btn btn-primary" value="/resource-items">
				<s:text name='back' />
			</s:a>
		</div>
		<div class="row">
			<s:actionerror />
			<s:actionmessage />
		</div>
	</div>
</body>
</html>