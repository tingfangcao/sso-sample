<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<html lang="en">
<head>
<%@ include file="head.jsp"%>
<title><s:text name="sso-app" /></title>
</head>
<body>
<div class="container" id="roleManagement-view">
	<s:label for="roleManagement-view">
		<s:text name="roleManagement"></s:text>-<s:text name="view"></s:text>
	</s:label>
	<div class="row">
		<div class="col-xs-4 col-sm-2">
			<label><s:text name="role.name"></s:text></label>
		</div>
		<div class="col-xs-8 col-sm-4">
			<s:property value="name" />
		</div>
		<div class="col-xs-4 col-sm-2">
			<label><s:text name="role.description"></s:text></label>
		</div>
		<div class="col-xs-8 col-sm-4">
			<s:property value="description" />
		</div>
	</div>
	<hr>
	
	<ec:tree
	identifier="id"
	parentAttribute="parentId"
	items="resourceItems"
	var="item"
	view="org.extremecomponents.tree.TreeView"
	filterable="false"
	sortable="false"
	>
	  <ec:row>
	    <ec:column property="#" title="sequence">${ROWCOUNT}</ec:column>
	    <ec:column property="name" title="resourceItem.name" cell="org.extremecomponents.tree.TreeCell"/>
	    <ec:column property="type" title="resourceItem.type"/>
	    <ec:column property="url" title="resourceItem.url"/>
	  </ec:row>
	</ec:tree>
		
	<div class="row">
		<s:a cssClass="btn btn-primary" value="/roles">
			<s:text name='back' />
		</s:a>
	</div>
	<div class="row">
		<s:actionerror />
		<s:actionmessage />
	</div>
</div>
</body>
</html>