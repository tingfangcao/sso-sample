package freedom.platform;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import freedom.platform.dao.UserDaoTest;

/**
 * @author Charlie Email:tingfangcao@163.com
 * @date 2020/09/03
 */
@RunWith(Suite.class)
@SuiteClasses({UserDaoTest.class})
public class AllTests {

}
