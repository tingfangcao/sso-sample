package freedom.platform;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Charlie Email:tingfangcao@163.com
 * @date 2020/09/03
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:/applicationContext.xml", "classpath:/beans-test.xml"})
public class SpringTest {

}
