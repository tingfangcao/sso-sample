package freedom.platform.dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import freedom.platform.SpringTest;
import freedom.platform.enums.YesOrNo;
import freedom.platform.model.User;

public class UserDaoTest extends SpringTest {

    @Autowired
    UserDao userDao;

    @Test
    public void testSaveS() {
        int start = ((Long)userDao.count()).intValue();
        User user;
        for (int i = 1; i <= 100; i++) {
            user = new User();
            user.setUsername("test" + (start + i));
            user.setPassword(user.getUsername());
            user.setLockFlag(YesOrNo.NO);
            user.mark(true);
            userDao.save(user);
        }
    }

}
